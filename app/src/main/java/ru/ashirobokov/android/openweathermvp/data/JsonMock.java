package ru.ashirobokov.android.openweathermvp.data;

import android.content.res.Resources;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import ru.ashirobokov.android.openweathermvp.R;
import ru.ashirobokov.android.openweathermvp.utils.ConstantManager;

/**
 * Created by ashirobokov on 02.12.2016.
 */
public class JsonMock {

    static final String TAG = ConstantManager.TAG_PREFIX + "JsonMock";

    private String jsonString;

    public JsonMock(InputStream in) {

        try {

//            InputStream in = res.openRawResource(R.raw.forecast212);

            byte[] b = new byte[in.available()];
            in.read(b);

            jsonString = new String(b, ConstantManager.UTF8);
            Log.d(TAG, "JSON:" + jsonString);

        } catch (IOException e) {
                e.printStackTrace();
        }

    }


    public JsonMock(Resources res) {
// this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;

        InputStream in = res.openRawResource(R.raw.forecast212);

        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        byte[] buffer = new byte[bufferSize];

        try {
            int len = 0;
            while ((in.available() > 0) && (len = in.read(buffer)) != -1) {
                        byteBuffer.write(buffer, 0, len);
            }


//      int size = byteBuffer.size();
//      int size = byteBuffer.toByteArray().length;
//      byte[] b = new byte[size];

        jsonString = new String(byteBuffer.toByteArray(), ConstantManager.UTF8);
        Log.d(TAG, "JSON:" + jsonString);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }


/*
*   Works good but not for android  :-)
*
*/
    private JsonMock(String path) {

        File jsonFile = new File(path);
        int size = (int) jsonFile.length();
        byte[] bytesEncoded = new byte[size];

        try {
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(jsonFile));
            in.read(bytesEncoded, 0, bytesEncoded.length);
            in.close();

            jsonString = new String(bytesEncoded, ConstantManager.UTF8);

        } catch (FileNotFoundException e) {
            Log.d(TAG, "Файл не найден по : " + path);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getJsonString() {

        return jsonString;
    }

}
