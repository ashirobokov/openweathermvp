package ru.ashirobokov.android.openweathermvp.di.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.ashirobokov.android.openweathermvp.service.NetworkOpenWeatherServiceGenerator;
import ru.ashirobokov.android.openweathermvp.service.OpenWeatherService;

/**
 * Created by Shirobokov on 26.03.2017.
 */

@Module
public class AppModule {

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    OpenWeatherService provideOpenWeatherService() {
        return NetworkOpenWeatherServiceGenerator.createNetworkRxService(OpenWeatherService.OPEN_WEATHER_SERVICE, OpenWeatherService.class);
    }

}
