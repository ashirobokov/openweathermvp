package ru.ashirobokov.android.openweathermvp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import ru.ashirobokov.android.openweathermvp.App;
import ru.ashirobokov.android.openweathermvp.R;
import ru.ashirobokov.android.openweathermvp.adapter.MainWeatherAdapter;
import ru.ashirobokov.android.openweathermvp.adapter.WeatherItemParcel;
import ru.ashirobokov.android.openweathermvp.di.components.DaggerMainComponent;
import ru.ashirobokov.android.openweathermvp.di.components.MainComponent;
import ru.ashirobokov.android.openweathermvp.di.modules.MainModule;
import ru.ashirobokov.android.openweathermvp.model.WeatherForecastItem;
import ru.ashirobokov.android.openweathermvp.mvp.IMainPresenter;
import ru.ashirobokov.android.openweathermvp.mvp.IMainView;
import ru.ashirobokov.android.openweathermvp.utils.ConstantManager;


public class MainActivity extends ProgessActivity implements IMainView, MainWeatherAdapter.WeatherItemListener {

    static final String TAG = ConstantManager.TAG_PREFIX + "MainActiv.";

    @Inject
    IMainPresenter mPresenter;

    private Toolbar mToolBar;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coordinator_layout);
        mToolBar = (Toolbar) findViewById(R.id.app_toolbar);
        setSupportActionBar(mToolBar);

        MainComponent mainComponent = DaggerMainComponent.builder()
                .appComponent(App.get().getComponent())
                .mainModule(new MainModule(this))
                .build();

        mainComponent.inject(this);

        mPresenter.loadWeatherForecast();

    }

    @Override
    public void showProgress() {
        super.showProgress();
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
    }

    @Override
    public void showWeatherForecast(List<WeatherForecastItem> list) {
        initRecyclerViewAdapter(list);
    }

    @Override
    public void navigateWeatherItemDetails(WeatherItemParcel parcelWeather) {

        Intent weatherIntent = new Intent(MainActivity.this, WeatherItemActivity.class);
        weatherIntent.putExtra(ConstantManager.PARCELABLE_KEY, parcelWeather);
        startActivity(weatherIntent);
    }

    @Override
    public void onWeatherItemClick(WeatherForecastItem forecastItem) {
        mPresenter.clickWeatherItem(forecastItem);
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.connection_error, Toast.LENGTH_LONG).show();
    }

    private void initRecyclerViewAdapter(List<WeatherForecastItem> list) {

        MainWeatherAdapter adapter = new MainWeatherAdapter(list, this, this);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(adapter);

    }


}
