package ru.ashirobokov.android.openweathermvp.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.ashirobokov.android.openweathermvp.di.annotations.PerActivity;
import ru.ashirobokov.android.openweathermvp.mvp.IMainPresenter;
import ru.ashirobokov.android.openweathermvp.mvp.IMainView;
import ru.ashirobokov.android.openweathermvp.mvp.MainPresenterImpl;
import ru.ashirobokov.android.openweathermvp.service.OpenWeatherService;
import ru.ashirobokov.android.openweathermvp.ui.MainActivity;

/**
 * Created by AShirobokov on 28.03.2017.
 */
@Module
public class MainModule {

    private MainActivity activity;

    public MainModule(MainActivity activity) {
            this.activity = activity;
    }

    @Provides
    @PerActivity
    IMainView providesIMainView() {
        return activity;
    }

    @Provides
    @PerActivity
    IMainPresenter provideIMainPresenter(IMainView view, OpenWeatherService openWeatherService) {
        return new MainPresenterImpl(view, openWeatherService);
    }

}
