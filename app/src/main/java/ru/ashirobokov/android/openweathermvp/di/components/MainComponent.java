package ru.ashirobokov.android.openweathermvp.di.components;

import dagger.Component;
import ru.ashirobokov.android.openweathermvp.di.annotations.PerActivity;
import ru.ashirobokov.android.openweathermvp.di.modules.MainModule;
import ru.ashirobokov.android.openweathermvp.model.Main;
import ru.ashirobokov.android.openweathermvp.ui.MainActivity;

/**
 * Created by Ashirobokov on 28.03.2017.
 */
@PerActivity
@Component(modules = MainModule.class, dependencies = AppComponent.class)
public interface MainComponent {

    void inject(MainActivity activity);

}
