package ru.ashirobokov.android.openweathermvp.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.ashirobokov.android.openweathermvp.R;
import ru.ashirobokov.android.openweathermvp.model.WeatherForecastItem;
import ru.ashirobokov.android.openweathermvp.utils.ConstantManager;

/**
 * Created by ashirobokov on 02.12.2016.
 */
public class MainWeatherAdapter extends RecyclerView.Adapter<MainWeatherAdapter.WeatherViewHolder> {

    private static final String TAG = ConstantManager.TAG_PREFIX + "Weather Adapter";

    private final Context context;
    private WeatherItemListener mListener;

    List<WeatherForecastItem> weathers = new ArrayList<>();

    protected class WeatherViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView weatherView;
        ImageView mPicture;
        TextView mDate;
        TextView mTemperature;
        TextView mDescription;

        WeatherViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            weatherView = (CardView) itemView.findViewById(R.id.card_view);
            mPicture = (ImageView) itemView.findViewById(R.id.weather_image);
            mDate = (TextView) itemView.findViewById(R.id.date_time);
            mTemperature = (TextView) itemView.findViewById(R.id.temperature);
            mDescription = (TextView) itemView.findViewById(R.id.weather_description);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            WeatherForecastItem forecastItem = weathers.get(position);
            Log.d(TAG, "pos = " + position );

            mListener.onWeatherItemClick(forecastItem);
        }

    }


    public MainWeatherAdapter(List<WeatherForecastItem> weathers, Context context, WeatherItemListener listener) {
            this.weathers = weathers;
            this.context = context;
            this.mListener = listener;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_weather, viewGroup, false);

        WeatherViewHolder weatherViewHolder = new WeatherViewHolder(view);

    return weatherViewHolder;
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder weatherViewHolder, int i) {

        String imageName = weathers.get(i).getWeather().get(0).getIcon();
        int imageId = context.getResources().getIdentifier("ic_"+imageName, "mipmap", context.getPackageName());
        weatherViewHolder.mPicture.setImageResource(imageId);

        int temperature = (int) Math.round(weathers.get(i).getMain().getTemp());

        weatherViewHolder.mDate.setText(weathers.get(i).getDtTxt());
        weatherViewHolder.mTemperature.setText(String.format("%d", temperature));
        weatherViewHolder.mDescription.setText(weathers.get(i).getWeather().get(0).getDescription());


        if (temperature < 0)
            weatherViewHolder.weatherView.setCardBackgroundColor(context.getResources().getColor(R.color.colorBlue));
        else
            weatherViewHolder.weatherView.setCardBackgroundColor(context.getResources().getColor(R.color.colorOrangeLight));

    }


    @Override
    public int getItemCount() {
        return weathers.size();
    }


    public interface WeatherItemListener {
        void onWeatherItemClick(WeatherForecastItem forecastItem);

    }

}
