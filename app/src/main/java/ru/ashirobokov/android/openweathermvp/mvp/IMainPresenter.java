package ru.ashirobokov.android.openweathermvp.mvp;

import ru.ashirobokov.android.openweathermvp.model.WeatherForecastItem;

/**
 * Created by AShirobokov on 27.03.2017.
 */
public interface IMainPresenter {

    void loadWeatherForecast();

    void clickWeatherItem(WeatherForecastItem weatherItem);

    void onDestroy();

}
