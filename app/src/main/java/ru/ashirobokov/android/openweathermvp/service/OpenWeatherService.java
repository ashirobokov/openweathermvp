package ru.ashirobokov.android.openweathermvp.service;

import retrofit2.Response;
import retrofit2.http.GET;
import ru.ashirobokov.android.openweathermvp.model.Forecast;
import rx.Observable;

/**
 * Created by AShirobokov on 15.12.2016.
 */
public interface OpenWeatherService {

    final String OPEN_WEATHER_SERVICE ="http://api.openweathermap.org/data/2.5/";

    @GET("forecast?id=524901&units=metric&lang=ru&APPID=d3a8d41e6e2303f3586d3581e46cd953")
    Observable<Response<Forecast>> getCurrentWeather();

}
