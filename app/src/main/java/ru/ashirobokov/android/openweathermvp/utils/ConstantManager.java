package ru.ashirobokov.android.openweathermvp.utils;

/**
 * Created by 1 on 02.12.2016.
 */
public interface ConstantManager {

    public boolean DEBUG = true;
    public static final String UTF8 = "UTF-8";
    public static final String JSON_FILE_PATH = "D:/Android/exchange/forecast212.json";

    public static final String TAG_PREFIX = "DEV ";
    public static final String PARCELABLE_KEY = "PARCELABLE_KEY";

}
