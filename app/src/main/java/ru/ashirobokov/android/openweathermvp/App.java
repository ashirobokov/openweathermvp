package ru.ashirobokov.android.openweathermvp;

import android.app.Application;
import android.content.Context;

import ru.ashirobokov.android.openweathermvp.di.components.AppComponent;
import ru.ashirobokov.android.openweathermvp.di.components.DaggerAppComponent;
import ru.ashirobokov.android.openweathermvp.di.modules.AppModule;

/**
 * Created by Shirobokov on 26.03.2017.
 */

public class App extends Application {

    private AppComponent mComponent;
    private Context mContext;
    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = createComponent();
        app = this;
        mContext = app.getApplicationContext();

    }

    public AppComponent createComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule())
                .build();
    }


    public AppComponent getComponent() {
        return mComponent;
    }

    public Context getContext() {
        return mContext;
    }

    public static App get() {
        return app;
    }

}
