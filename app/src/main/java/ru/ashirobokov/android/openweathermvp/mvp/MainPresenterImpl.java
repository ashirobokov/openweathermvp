package ru.ashirobokov.android.openweathermvp.mvp;

import android.util.Log;

import java.util.List;

import retrofit2.Response;
import ru.ashirobokov.android.openweathermvp.adapter.WeatherItemParcel;
import ru.ashirobokov.android.openweathermvp.model.Forecast;
import ru.ashirobokov.android.openweathermvp.model.WeatherForecastItem;
import ru.ashirobokov.android.openweathermvp.service.OpenWeatherService;
import ru.ashirobokov.android.openweathermvp.utils.ConstantManager;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by AShirobokov on 28.03.2017.
 */
public class MainPresenterImpl implements IMainPresenter {

    private IMainView mView;
    private OpenWeatherService mService;

    static final String TAG = ConstantManager.TAG_PREFIX + "MainPresenter";


    public MainPresenterImpl(IMainView mainView, OpenWeatherService service) {
        this.mView = mainView;
        this.mService = service;

    }

    @Override
    public void loadWeatherForecast() {

        mView.showProgress();

        Observable<Response<Forecast>> weather = mService.getCurrentWeather();

        weather.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<Forecast>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "Completed :-)");
                    }

                    @Override
                    public void onError(Throwable t) {
                        mView.hideProgress();
                        mView.showError();
                        Log.d(TAG, t.toString());
                    }

                    @Override
                    public void onNext(Response<Forecast> response) {
                        if (response.code() == 200) {

                            try {

                                List<WeatherForecastItem> mList = response.body().getList();
                                Log.d(TAG, mList.toString());
                                mView.showWeatherForecast(mList);

                            } catch (NullPointerException e) {
                                Log.d(TAG, e.toString());
                            }

                        } else if (response.code() == 404) {
                            mView.showError();
                            Log.d(TAG, "Ресурс не найден");
                        } else {
                            Log.d(TAG, "Другая ошибка");
                            if (ConstantManager.DEBUG) {
                                Log.d(TAG, String.valueOf(response.code()));
                            }

                        }
                        mView.hideProgress();
                    }

                });

    }

    @Override
    public void onDestroy() {
        mView = null;
    }

    @Override
    public void clickWeatherItem(WeatherForecastItem forecastItem) {

        WeatherItemParcel weatherItem = getWeatherItem(forecastItem);
        mView.navigateWeatherItemDetails(weatherItem);

    }

    private WeatherItemParcel getWeatherItem (WeatherForecastItem weather) {

        String dateTime = weather.getDtTxt();
        Double temperature = weather.getMain().getTemp();
        String weatherImage = weather.getWeather().get(0).getIcon();
        String weatherDescription = weather.getWeather().get(0).getDescription();
        Double windSpeed = weather.getWind().getSpeed();
        Double windDeg = weather.getWind().getDeg();
        Integer humidity = weather.getMain().getHumidity();
        Double pressure = weather.getMain().getPressure();
        Integer cloudiness = weather.getClouds().getAll();

        WeatherItemParcel data = new WeatherItemParcel(dateTime, temperature, weatherImage, weatherDescription,
                windSpeed, windDeg, humidity, pressure, cloudiness);


        return data;
    }

}
