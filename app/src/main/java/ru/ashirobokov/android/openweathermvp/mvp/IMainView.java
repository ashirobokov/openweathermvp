package ru.ashirobokov.android.openweathermvp.mvp;

import java.util.List;

import ru.ashirobokov.android.openweathermvp.adapter.WeatherItemParcel;
import ru.ashirobokov.android.openweathermvp.model.WeatherForecastItem;

/**
 * Created by 1 on 28.03.2017.
 */
public interface IMainView {

    void showProgress();
    void hideProgress();
    void showWeatherForecast(List<WeatherForecastItem> list);
    void navigateWeatherItemDetails(WeatherItemParcel data);
    void showError();

}
