package ru.ashirobokov.android.openweathermvp.di.components;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Component;
import ru.ashirobokov.android.openweathermvp.di.modules.AppModule;
import ru.ashirobokov.android.openweathermvp.service.OpenWeatherService;

/**
 * Created by Shirobokov on 26.03.2017.
 */

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    Gson gson();
    OpenWeatherService openWeatherService();

}
