package ru.ashirobokov.android.openweathermvp.service;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Shirobokov on 26.03.2017.
 */

public class NetworkOpenWeatherServiceGenerator {

    public static <S> S createNetworkRxService(String serviceUrl, Class<S> serviceClass) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(serviceClass);

    }

}