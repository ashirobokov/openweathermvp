package ru.ashirobokov.android.openweathermvp.di.annotations;

import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import javax.inject.Scope;

/**
 * Created by AShirobokov on 28.03.2017.
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {
}
