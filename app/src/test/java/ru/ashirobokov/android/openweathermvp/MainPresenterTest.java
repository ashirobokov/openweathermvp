package ru.ashirobokov.android.openweathermvp;

import android.content.res.Resources;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.List;

import ru.ashirobokov.android.openweathermvp.model.WeatherForecastItem;
import ru.ashirobokov.android.openweathermvp.mvp.IMainPresenter;
import ru.ashirobokov.android.openweathermvp.mvp.IMainView;
import ru.ashirobokov.android.openweathermvp.mvp.MainPresenterImpl;
import ru.ashirobokov.android.openweathermvp.service.OpenWeatherService;

/**
 * Created by AShirobokov on 30.03.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {


    @Mock
    private List<String> mockArrayList;


    @Mock
    private IMainView mView;
    @Mock
    private OpenWeatherService mService;
    @Mock
    private IMainPresenter mPresenter;

    private List<WeatherForecastItem> forecastItems;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mPresenter = new MainPresenterImpl(mView, mService);

    }

    @Test
    public void checkArrayList() {

        when(mockArrayList.get(0)).thenReturn("Hello world");

        String result = mockArrayList.get(0);

        assertEquals("Should have the correct string", "Hello world", result);

        verify(mockArrayList).get(0);
    }

    @Test
    public void testLoadWeatherForecast() {
        /* TBD */

    }


}


